// ==UserScript==
// @name           Reddit to old redirector
// @namespace      Zera's userscripts
// @match          http://reddit.com/*
// @match          https://reddit.com/*
// @match          http://www.reddit.com/*
// @match          https://www.reddit.com/*
// @run-at         document-start
// ==/UserScript==

url = location.href
url = url.replace(/\bwww\.\b/, "")
url = url.replace("reddit.com","old.reddit.com")
location.href = url
