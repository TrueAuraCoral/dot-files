// ==UserScript==
// @name           Youtube to Invidious Redirector
// @namespace      Zera's userscripts
// @match          http://youtube.com/*
// @match          https://youtube.com/*
// @match          http://www.youtube.com/*
// @match          https://www.youtube.com/*
// @run-at         document-start
// ==/UserScript==

location.href=location.href.replace("youtube.com","yewtu.be");
