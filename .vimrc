" COLOR STUFF
set guifont=Hack:h12:cANSI:qDRAFT
set guifont=Consolas:h11
colorscheme evening
syntax on

" Show number
set number

" Map leader
let mapleader = ","

" No backup stuff
set noswapfile
set nobackup

" Disable the current stuff that is highlighted
map <silent> <C-h> :noh<cr>

" Searching but better
" Ignore case when searching
set ignorecase
" When searching try to be smart about cases 
set smartcase
" Highlight search results
set hlsearch
" Makes search act like search in modern browsers
set incsearch 

"set cursorline
set cursorline
set cursorcolumn
highlight CursorLine ctermbg=Yellow cterm=bold guibg=#3b3b3b
highlight CursorColumn ctermbg=Yellow cterm=bold guibg=#3b3b3b

" Copy paste 
" NOTE: These are clipboard settings for windoze. Linux should be different.
set paste
set clipboard=unnamed

" Show text in middle
set sidescrolloff=999
set scrolloff=999

" No more stinky esc key that's way to far away. Instead I can use alt + space.
imap <M-Space> <Esc>

" Surperior jkl; rather then hjkl
noremap j h
noremap k j
noremap l k
noremap ; l

" These autoclose stuff
inoremap ( ()<Esc>i
inoremap { {}<Esc>i
inoremap {<CR> {<CR>}<Esc>O
inoremap [ []<Esc>i
inoremap < <><Esc>i
inoremap ' ''<Esc>i
inoremap " ""<Esc>i

" Auto remove spaces on save
autocmd BufWritePre * %s/\s\+$//e

" Tab stuffs
" Use spaces instead of tabs
set expandtab
" Be smart when using tabs ;)
set smarttab
" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4
" Autoindents
set autoindent
set smartindent
set backspace=indent,eol,start

" Status line
" Give some space before the line starts
set laststatus=2
" Actually start the status line parameters
set statusline= 
" Show the file name
set statusline+=\ FILE:
set statusline+=\ %F
" Show both the current line number and the total lines in file
set statusline+=\ LINES:
set statusline+=\ %l
set statusline+=\ %L
" Go to the right side of the status line
set statusline+=%=
" Show the progress percentage of where you are in the document
set statusline+=%p
